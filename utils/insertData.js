const db = require('../app/models');

const now = new Date();

const alumnos = [
  {
    cod_alumno:'1234567',
    dni:'12345678',
    nombres: 'Maria Jose',
    apellidos: 'Salas Inga',
    escuela: 'Sistemas',
    fotografia: 'https://limonpimg.s3.us-east-2.amazonaws.com/img_web/plato01.jpg'
  },
  {
    cod_alumno:'1234568',
    dni:'12345678',
    nombres: 'Lola',
    apellidos: 'Mento De Verdad',
    escuela: 'Sistemas',
    fotografia: 'https://limonpimg.s3.us-east-2.amazonaws.com/img_web/plato02.jpg'
  },
  {
    cod_alumno:'1234569',
    dni:'12345678',
    nombres: 'Javier',
    apellidos: 'Villaizan Morales',
    escuela: 'Sistemas',
    fotografia: 'https://limonpimg.s3.us-east-2.amazonaws.com/img_web/plato03.jpg'
  },
  {
    cod_alumno:'1234510',
    dni:'12345678',
    nombres: 'Eduardo',
    apellidos: 'Practican Tevago',
    escuela: 'Sistemas',
    fotografia: 'https://limonpimg.s3.us-east-2.amazonaws.com/img_web/plato04.jpg'
  },
];

async function insertData() {
  console.log('Iniciando la insercion de tablas');
  console.log('-----------------------------');
  // alumnos
  for (const alumno of alumnos) {
    await db.Alumno.create(alumno);
  }
   console.log('-----------------------------');
  console.log('Insercion de tablas finalizado');
}

// ejecucion de la funcion
insertData();