var express = require('express');
var router = express.Router();


const alumnoRouter = require('./alumnos.routes');


/* GET home page. */
router.get('/', function (req, res, next) {
  response.header("Access-Control-Allow-Origin", "*");
  response.header("Access-Control-Allow-Headers", "Content-Type");
  res.render('index', { title: 'Bienvenido a la API' });
});


router.use('/alumnos', alumnoRouter);



module.exports = router;
