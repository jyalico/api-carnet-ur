const express = require('express');
const router = express.Router();
const alumnoCtrl = require('../controllers/alumnos.controller');

/* GET home page. */
router.get('/', alumnoCtrl.list);
router.get('/:id', alumnoCtrl.get);
module.exports = router;