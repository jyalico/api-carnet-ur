'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('Alumno', {
    cod_alumno: {
      type: DataTypes.INTEGER,
      primaryKey:true,
    },
    dni: DataTypes.INTEGER,
    nombres: DataTypes.STRING(50),
    apellidos: DataTypes.STRING(50),
    escuela: DataTypes.STRING(50),
    fotografia: DataTypes.STRING,
  },// {schema: 'public'}
  );
  return User;
};