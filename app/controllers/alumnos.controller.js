const db = require('../models');
const Alumno = db.Alumno;

exports.list = async function(req, res) {
  try {
    const alumnos = await Alumno.findAll();
    res.json(alumnos);
  } catch(err) {
    res.status(500).json({error: err});
  }
}

exports.get = async function(req, res) {
  try {
    const id = req.params.id;
    const alumno = await Alumno.findOne(
      {
        //Select nombre, apellidos, email, dni from 
        where: {cod_alumno: id},
        attributes: ['cod_alumno','dni','nombres', 'apellidos', 'escuela','fotografia'],
      }
    );
    if(alumno){
      res.json(alumno);
    } else{
      res.status(404).json({error: 'not found'})
    } 
  } catch(err) {
    res.status(500).json({error: err});
  }
}
